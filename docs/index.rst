.. Sugar Con Cache documentation master file, created by
   sphinx-quickstart on Sat Apr 25 14:18:32 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Sugar Con Cache's documentation!
===========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   content/basics
   content/classes

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
