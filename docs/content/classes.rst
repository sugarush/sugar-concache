Classes
=======

.. autoclass:: sugar_concache.redis.RedisDB
  :members:

.. autoclass:: sugar_concache.postgres.PostgresDB
  :members:

.. autoclass:: sugar_concache.mongo.MongoDB
  :members:

.. autoclass:: sugar_concache.rethink.RethinkDB
  :members:
