About
=====

Sugar Con Cache provides connection caching classes for **Redis**, **MongoDB**, **PostgreSQL** and **RethinkDB**.

Source
------

The project's `source <https://gitlab.com/sugarush/sugar-concache>`_ is
available on `GitLab`.

Installation
------------

Suagr Con Cache can be installed with `pip`.

``pip install git+https://gitlab.com/sugarush/sugar-concache@master``

Usage
-----

.. code-block:: python

  import asyncio
  from sugar_concache.redis import RedisDB

  RedisDB.defaults = {
    'host': 'redis://localhost'
  }

  async def main():
  
    await RedisDB.set_event_loop(asyncio.get_event_loop())

    alpha = await RedisDB.connect()
    beta = await RedisDB.connect()
    gamma = await RedisDB.connect(host='redis://not-localhost')

    assert alpha is beta
    assert alpha is not gamma

  asyncio.run(main())
