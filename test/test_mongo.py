import asyncio

from unittest import TestCase

from sugar_concache.mongo import MongoDB


class MongoDBTest(TestCase):

    def test_connect(self):
        a = MongoDB.connect(host='localhost')
        b = MongoDB.connect(host='localhost')
        c = MongoDB.connect(host='localhost', ssl=False)
        self.assertIs(a, b)
        self.assertIsNot(a, c)

    def test_change_loop(self):
        connection = MongoDB.connect()
        loop = asyncio.get_event_loop()
        MongoDB.set_event_loop(loop)
        self.assertDictEqual(MongoDB.connections, { })
