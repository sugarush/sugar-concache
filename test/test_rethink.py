from sugar_asynctest import AsyncTestCase

from sugar_concache.rethink import RethinkDB


class RethinkDBTest(AsyncTestCase):

    default_loop = True

    async def test_connect(self):
        a = await RethinkDB.connect()
        b = await RethinkDB.connect(host='localhost')
        c = await RethinkDB.connect(host='localhost')

        self.assertIs(b, c)
        self.assertIsNot(a, c)

        await RethinkDB.close()
