import asyncio

from sugar_asynctest import AsyncTestCase

from sugar_concache.redis import RedisDB


class RedisDBTest(AsyncTestCase):

    default_loop = True

    async def test_connect(self):
        loop = asyncio.get_event_loop()
        await RedisDB.set_event_loop(loop)
        a = await RedisDB.connect(host='redis://localhost')
        b = await RedisDB.connect(host='redis://localhost')
        c = await RedisDB.connect(host='redis://localhost', ssl=False)
        self.assertIs(a, b)
        self.assertIsNot(a, c)

    async def test_change_loop(self):
        loop = asyncio.get_event_loop()
        await RedisDB.set_event_loop(loop)
        connection = await RedisDB.connect(host='redis://localhost')
        loop = asyncio.get_event_loop()
        await RedisDB.set_event_loop(loop)
        self.assertDictEqual(RedisDB.connections, { })
