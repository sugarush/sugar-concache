import asyncio

from sugar_asynctest import AsyncTestCase

from sugar_concache.postgres import PostgresDB


class PostgresDBTest(AsyncTestCase):

    default_loop = True

    async def test_connect(self):
        a = await PostgresDB.connect(host='localhost', database='postgres')
        b = await PostgresDB.connect(host='localhost', database='postgres')
        c = await PostgresDB.connect(host='localhost', database='postgres', port=5432)

        self.assertIs(a, b)
        self.assertIsNot(a, c)

    async def test_change_loop(self):
        connection = await PostgresDB.connect(host='localhost', database='postgres')
        loop = asyncio.get_event_loop()
        await PostgresDB.set_event_loop(loop)
        self.assertDictEqual(PostgresDB.connections, { })
