from . util import serialize

try:
    from motor.motor_asyncio import AsyncIOMotorClient

    class MongoDB(object):
        '''
        The MongoDB connection cache.
        '''

        defaults = { }
        connections = { }
        loop = None

        @classmethod
        def connect(cls, **kargs):
            kargs['connect'] = True
            kargs.update(cls.defaults)
            key = serialize(kargs)

            if cls.loop:
                kargs['io_loop'] = cls.loop

            connection = cls.connections.get(key)
            if connection:
                return connection

            cls.connections[key] = AsyncIOMotorClient(**kargs)
            return cls.connections[key]

        @classmethod
        def close(cls):
            for key in cls.connections:
                cls.connections[key].close()
            cls.connections = { }

        @classmethod
        def set_event_loop(cls, loop):
            cls.loop = loop
            cls.close()
except ImportError as e:
    raise e
