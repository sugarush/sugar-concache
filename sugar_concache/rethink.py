from . util import serialize

try:
    from rethinkdb import RethinkDB
    r = RethinkDB()
    r.set_loop_type('asyncio')

    class RethinkDB(object):
        '''
        The RethinkDB connection cache.
        '''

        defaults = { }
        connections = { }

        @classmethod
        async def connect(cls, **kargs):
            kargs.update(cls.defaults)
            key = serialize(kargs)

            connection = cls.connections.get(key)
            if connection:
                return connection

            cls.connections[key] = await r.connect(**kargs)
            return cls.connections[key]

        @classmethod
        async def close(cls):
            for key in cls.connections:
                await cls.connections[key].close()
            cls.connections = { }
except ImportError as e:
    raise e
