import asyncio

from . util import serialize

try:
    from aioredis import create_pool, create_redis_pool

    class RedisDB(object):
        '''
        The Redis connection cache.
        '''

        defaults = { }
        connections = { }
        loop = None

        @classmethod
        async def connect(cls, **kargs):
            '''
            Connect to Redis using `\*\*kargs`.
            '''
            kargs.update(cls.defaults)
            key = serialize(kargs)

            host = kargs.pop('host')
            if not host:
                raise Exception('RedisDB.connect: No host provided.')

            if not cls.loop:
                raise Exception('RedisDB.connect: Event loop not yet set.')

            kargs['loop'] = cls.loop

            connection = cls.connections.get(key)
            if connection:
                return connection

            if kargs.get('lowlevel'):
                del kargs['lowlevel']
                cls.connections[key] = await create_pool(host, **kargs)
                return cls.connections[key]
            else:
                cls.connections[key] = await create_redis_pool(host, **kargs)
                return cls.connections[key]

        @classmethod
        async def set_event_loop(cls, loop):
            '''
            Set the Redis event loop.
            '''
            await cls.close()
            cls.loop = loop
            cls.connections = { }

        @classmethod
        async def close(cls):
            '''
            Close all existing Redis connections.
            '''
            for connection in cls.connections:
                cls.connections[connection].close()
                await cls.connections[connection].wait_closed()

except ImportError as e:
    raise e
