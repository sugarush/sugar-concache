from . util import serialize

try:
    from asyncpg import create_pool

    class PostgresDB(object):
        '''
        The PostgreSQL connection cache.
        '''

        defaults = { }
        connections = { }
        loop = None

        @classmethod
        async def connect(cls, **kargs):
            kargs.update(cls.defaults)
            key = serialize(kargs)

            connection = cls.connections.get(key)
            if connection:
                return connection

            cls.connections[key] = await create_pool(**kargs)
            return cls.connections[key]

        @classmethod
        async def close(cls):
            for key in cls.connections:
                await cls.connections[key].close()
            cls.connections = { }

        @classmethod
        async def set_event_loop(cls, loop):
            cls.loop = loop
            await cls.close()
except ImportError as e:
    raise e
