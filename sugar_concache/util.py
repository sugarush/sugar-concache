import json

def serialize(dictionary):
    return json.dumps(dictionary, separators=(',', ':'), sort_keys=True)
