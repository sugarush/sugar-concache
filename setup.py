__author__ = 'lux'

from setuptools import setup

setup(
    name='sugar-concache',
    version='0.0.1',
    author='lux',
    author_email='lux@sugarush.io',
    url='https://gitlab.com/sugarush/sugar-concache',
    packages=[
        'sugar_concache'
    ],
    description='Connection caching for Redis, MongoDB, PostgreSQL and RethinkDB.',
    install_requires=[
        'sugar-asynctest@git+https://gitlab.com/sugarush/sugar-asynctest@master'
    ]
)
